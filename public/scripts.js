var jsmediatags = window.jsmediatags;

let rafID = null;
let analyser = null;
let c = null;
let cDraw = null;
let ctx = null;
let microphone = null;
let ctxDraw = null;

let loader,filename;

let fileChosen = false;
let hasSetupUserMedia = false;

const audioContext = AudioContext || webkitAudioContext;
const context = new audioContext();

const playExample = document.getElementById('playExample');
const input = document.getElementById('input');
const loading = document.getElementById('loading');

const songInfoWrapper = document.getElementById('song_info_wrapper');
const title = document.getElementById('title');
const artist = document.getElementById('artist');
const album = document.getElementById('album');

if (!window.requestAnimationFrame)
    window.requestAnimationFrame = window.webkitRequestAnimationFrame;

playExample.addEventListener('click', () => {
    playSample();
});

input.addEventListener('change', event => {
    fileHandler(event);
});

document.addEventListener('DOMContentLoaded', () => {
    loader = new BufferLoader();
    initBinCanvas();
});

function fileHandler(event) {
    const file = event.target.files[0];
    
    fileChosen = true;
    setupAudioNodes();

    jsmediatags.read(file, {
        onSuccess: tag => {
            const tags = tag.tags;
            title.textContent = tags.title;
            artist.textContent = tags.artist;
            album.textContent = tags.album;
            if (tags.title.length > 14 && tags.title.length <= 17) {
				title.style.fontSize = '1.75vw';
            }
            if (tags.title.length > 17 && tags.title.length <= 20) {
				title.style.fontSize = '1.25vw';
			}
			if (tags.title.length > 20) {
				title.style.fontSize = '1vw';
				
			}
            songInfoWrapper.style.visibility = 'visible';
        },
        onError: error => {
            console.log(error);
        }
    });
    
    const request = new XMLHttpRequest();
    const url = URL.createObjectURL(file);

    request.addEventListener("progress", updateProgress);
	request.addEventListener("load", transferComplete);
	request.addEventListener("error", transferFailed);
    request.addEventListener("abort", transferCanceled);
    
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    request.onload = () => {
        context.decodeAudioData(request.response, buffer => {
            sourceNode.buffer = buffer;
            sourceNode.start(0);
            document.body.classList.add('animateHue');
            document.getElementById('backgroundHue').classList.add('animateHue');
        }, (e) => {
            console.log(e);
        });
    }
    request.send();
}

function playSample() {
    fileChosen = true;
    setupAudioNodes();

    const request = new XMLHttpRequest();

    request.addEventListener("progress", updateProgress);
    request.addEventListener("load", transferComplete);
    request.addEventListener("error", transferFailed);
    request.addEventListener("abort", transferCanceled);

    request.open('GET', './nandemonaiya.mp3', true);
    request.responseType = 'arraybuffer';

    request.onload = () => {
        context.decodeAudioData(request.response, buffer => {
            title.textContent = "Nandemonaiya";
            artist.textContent = "RADWIMPS";
            album.textContent = "Your Name";

            songInfoWrapper.style.visibility = "visible";

            sourceNode.buffer = buffer;
            sourceNode.start(0);
            document.body.classList.add('animateHue');
            document.getElementById('backgroundHue').classList.add('animateHue');
        }, (e) => {
            console.log(e);
        });
    }

    request.send();
}

function updateProgress(oEvent) {
    if (oEvent.lengthComputable) {
        const percentComplete = oEvent.loaded / oEvent.total;

        //Disable the Upload Buttons
        input.disabled = true;
        playExample.disabled = true;

        loading.textContent = `Loading music file ... ${Math.floor(percentComplete * 100)}%`;
        console.log(`Loading music file ... ${Math.floor(percentComplete * 100)}%`);
    } else {
        console.log('Unable to compute progress info.')
    }
}

function transferComplete(){
    console.log("The transfer is complete");
    loading.textContent = 'File transfer complete';
}

function transferFailed(){
    console.log("An error has occured while transferring the file.");
    loading.textContent = "Loading failed";
    input.disabled = false;
    playExample.disabled = false;
}
function transferCanceled(){
    console.log("The file transfer has been cancelled by the user.");
    loading.textContent = "Loading canceled";
}

function initBinCanvas() {
    c = document.getElementById('freq');
    c.width = window.innerWidth;
    c.height = window.innerHeight;

    ctx = c.getContext("2d");

    ctx.canvas.width = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    window.addEventListener( 'resize', onWindowResize, false);

    let gradient = ctx.createLinearGradient(0, c.height - 300, 0, window.innerHeight - 25);
    gradient.addColorStop(1, '#00f');
    gradient.addColorStop(0.75, '#f00');
    gradient.addColorStop(0.25, '#f00');
    gradient.addColorStop(0, '#ffff00');

    ctx.fillStyle = "#9c0001";
}

function onWindowResize() {
    ctx.canvas.width = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    //some title shit
    // const containerHeight = $("#song_info_wrapper").height();
    // const topVal = $(window).height() / 2 - containerHeight / 2;
    // $("song_info_wrapper").css("top", topVal);
    // console.log(topVal);
}

let audioBuffer;
let sourceNode;

function setupAudioNodes() {
    analyser = context.createAnalyser();
    sourceNode = context.createBufferSource();
    sourceNode.connect(analyser);
    sourceNode.connect(context.destination);
    rafID = window.requestAnimationFrame(updateVisualization);
}

function reset() {
    if (typeof sourceNode !== 'undefined') {
        sourceNode.stop(0);
    }
}

function updateVisualization() {
    if (fileChosen || hasSetupUserMedia) {
        const array = new Uint8Array(analyser.frequencyBinCount);
        analyser.getByteFrequencyData(array);

        drawBars(array);
    }

    rafID = window.requestAnimationFrame(updateVisualization);
}

function drawBars(array) {
    
    // Show bins over threshold
    const threshold = 0;
    // Clear current state
    ctx.clearRect(0, 0, c.width, c.height);
    //Max count of bits for the visualization
    const maxBinCount = array.length;
    //Space between bins
    const space = 3;

    ctx.save();

    ctx.globalCompositeOperation = 'source-over';

    ctx.scale(0.5, 0.5);
    ctx.translate(window.innerWidth, window.innerHeight);
    ctx.fillStyle = '#FFF';

    const bass = Math.floor(array[1]);
    const radius = 0.45 * $(window).width() <= 300 ? -(bass * 0.5 + 0.45 * $(window).width()) : -(bass * 0.5 + 300);

    let bar_length_factor = 1;
    if ($(window).width() >= 785) {
        bar_length_factor = 1.0;
    } else if ($(window).width() < 785) {
        bar_length_factor = 1.5;
    } else if ($(window).width() < 500) {
        bar_length_factor = 20.0;
    }
    
    for (let i = 0; i < maxBinCount; i++) {
        let value = array[i];
        if (value >= threshold) {
            ctx.fillRect(0, radius, $(window).width() <= 450 ? 2 : space, -value / bar_length_factor);
            ctx.rotate((180 / 128) * Math.PI/180);
        }
    }

    for (let i = 0; i < maxBinCount; i++) {
        let value = array[i];
        if (value >= threshold) {
            ctx.rotate((180 / 128) * Math.PI/180);
            ctx.fillRect(0, radius, $(window).width() <= 450 ? 2 : space, -value / bar_length_factor);
        }
    }

    ctx.restore();
}