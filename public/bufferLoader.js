let reader;

window.AudioContext = window.AudioContext || window.webkitAudioContext;

class BufferLoader {
    constructor (sources) {
        this.buffers = {};
        this.context = null;
        this.buffer = null;
        this.init();
    }

    init() {
        try {
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            this.context = new AudioContext();
        } catch(_) {
            alert('Web Audio API is not supported in this browser');
        }
    }

    onBufferLoadError(_) {
        console.error('Error loading buffer');
    }

    onBufferLoad(bufferName, srcBuffer, callback) {
        this.context.decodeAudioData(srcBuffer, function onSuccess(buffer) {
            this.buffers[bufferName] = buffer;
            if (typeof callback === 'function') {
                callback();
            }
        }.bind(this), this.onBufferError);
    }

    load(bufferName, file, callback) {
        reader = new FileReader();
        reader.onload = function(data) {
            if (data.target && data.target.result) {
                this.onBufferLoad(bufferName, data.target.result, callback);
            } else {
                console.dir(data);
            }
        }.bind(this);
        reader.readAsArrayBuffer(file);
    }

    _playBuffer(name, gain, time) {
        let source = this.context.createBufferSource();
        source.buffer = this.buffer;

        let analyser = this.context.createAnalyser();

        source.connect(analyser);
        source.connect(this.context.destination);
        source.start(time);
    }

    play(name, gain, time) {
        gain = typeof gain !== 'undefined' ? gain : 1;
        time = typeof time !== 'undefined' ? gain : 0;

        this.buffer = this.buffers[name];
        if (this.buffer) { this._playBuffer(name, time, gain); }
        else { throw new Error("Buffer does not exist"); }
    }
}